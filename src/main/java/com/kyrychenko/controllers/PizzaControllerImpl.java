package com.kyrychenko.controllers;

import com.kyrychenko.model.Order;
import com.kyrychenko.model.PizzaOrderModel;
import com.kyrychenko.model.PizzaOrderModelInterface;
import com.kyrychenko.model.pizzas.Pizza;

import java.util.List;

public class PizzaControllerImpl implements PizzaController {
    private PizzaOrderModelInterface model;

    public PizzaControllerImpl() {
        this.model = new PizzaOrderModel();
    }

    @Override
    public List<Pizza> orderPizza(Order order) {
        return model.preparePizzas(order);
    }
}

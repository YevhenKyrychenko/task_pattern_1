package com.kyrychenko.controllers;

import com.kyrychenko.model.Order;
import com.kyrychenko.model.pizzas.Pizza;

import java.util.List;

public interface PizzaController {
    List<Pizza> orderPizza(Order order);
}

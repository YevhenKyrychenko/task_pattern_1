package com.kyrychenko.view;

import com.kyrychenko.controllers.PizzaController;
import com.kyrychenko.controllers.PizzaControllerImpl;
import com.kyrychenko.model.Order;
import com.kyrychenko.model.domain.PizzaType;
import com.kyrychenko.model.orders.OrderItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class PizzaView {
    private PizzaController controller;

    public PizzaView() {
        this.controller = new PizzaControllerImpl();
    }

    public void view() {
        OrderItem item = new OrderItem(PizzaType.PEPPERONI,2);
        Order order = new Order(Arrays.asList(item));
        System.out.println(controller.orderPizza(order));
        System.out.println(controller.orderPizza(order).stream().mapToDouble(p -> p.getPrice()).sum());
    }
}

package com.kyrychenko.model.pizzas;

import com.kyrychenko.model.domain.Toppings;

public class Ingredient {
    private double price;
    private Toppings topping;

    public Ingredient(Toppings topping, double price) {
        this.price = price;
        this.topping = topping;
    }

    public double getPrice() {
        return price;
    }

    public Toppings getTopping() {
        return topping;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "price=" + price +
                ", topping=" + topping +
                '}';
    }
}

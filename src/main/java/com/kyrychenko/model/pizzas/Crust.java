package com.kyrychenko.model.pizzas;

import com.kyrychenko.model.domain.CrustType;

public class Crust {
    private double price;
    private CrustType crustType;

    public Crust(double price, CrustType crustType) {
        this.price = price;
        this.crustType = crustType;
    }

    public double getPrice() {
        return price;
    }

    public CrustType getCrustType() {
        return crustType;
    }

    @Override
    public String toString() {
        return "Crust{" +
                "price=" + price +
                ", crustType=" + crustType +
                '}';
    }
}

package com.kyrychenko.model.pizzas;

import java.util.List;

public class Pizza {
    private Double price;
    private List<Ingredient> ingredients;
    private Sauce sauce;
    private Crust dough;

    public Pizza(List<Ingredient> ingredients, Sauce sauce, Crust dough) {
        this.ingredients = ingredients;
        this.sauce = sauce;
        this.dough = dough;
    }

    public Double getPrice() {
        if (this.price == null) {
            this.price = this.calculatePrice();
        }
        return this.price;
    }

    private Double calculatePrice() {
        return this.ingredients.stream().mapToDouble(i -> i.getPrice()).sum()
                + sauce.getPrice() + dough.getPrice();
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "price=" + calculatePrice() +
                ", ingredients=" + ingredients +
                ", sauce=" + sauce +
                ", dough=" + dough +
                '}';
    }
}

package com.kyrychenko.model.pizzas;

import com.kyrychenko.model.domain.SauceType;

public class Sauce {
    private double price;
    private SauceType sauceType;

    public Sauce(SauceType sauceType, double price) {
        this.price = price;
        this.sauceType = sauceType;
    }

    public double getPrice() {
        return price;
    }

    public SauceType getSauceType() {
        return sauceType;
    }

    @Override
    public String toString() {
        return "Sauce{" +
                "price=" + price +
                ", sauceType=" + sauceType +
                '}';
    }
}

package com.kyrychenko.model.domain;

public enum Toppings {
    CHEESE, SALAMI, PEPPERONI, MUSHROOMS, ONIONS, SAUSAGE, BACON, PINEAPPLE, PEPPERS
}

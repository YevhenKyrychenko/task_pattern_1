package com.kyrychenko.model.domain;

public enum SauceType {
    MARINARA, PESTO, PLUM_TOMATO
}

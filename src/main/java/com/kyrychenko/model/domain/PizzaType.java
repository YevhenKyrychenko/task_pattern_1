package com.kyrychenko.model.domain;

public enum PizzaType {
    CUSTOM, PEPPERONI
}

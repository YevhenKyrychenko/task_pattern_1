package com.kyrychenko.model.domain;

public enum CrustType {
    THICK, THIN
}

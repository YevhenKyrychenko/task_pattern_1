package com.kyrychenko.model;

import com.kyrychenko.model.domain.PizzaType;
import com.kyrychenko.model.factories.PizzaFactory;
import com.kyrychenko.model.factories.PizzaFactoryImpl;
import com.kyrychenko.model.orders.OrderItem;
import com.kyrychenko.model.pizzas.Pizza;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PizzaOrderModel implements PizzaOrderModelInterface {
    private PizzaFactory factory;

    public PizzaOrderModel() {
        this.factory = new PizzaFactoryImpl();
    }

    @Override
    public List<Pizza> preparePizzas(Order order) {
        return order.getOrderItems().stream()
                .flatMap(o -> preparePizzas(o))
                .collect(Collectors.toList());
    }

    private Stream<Pizza> preparePizzas(OrderItem item) {
        if(item.getType() == PizzaType.PEPPERONI) {
           return IntStream
                   .range(0, item.getCount())
                   .mapToObj(i -> factory.bakePepperoni())
                   ;
        }
        return IntStream
                .range(0, item.getCount())
                .mapToObj(i -> factory.bakeCustomPizza(item.getIngredients(), item.getSauce(), item.getCrust()))
                ;
    }
}

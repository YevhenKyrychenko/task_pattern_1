package com.kyrychenko.model.factories;

import com.kyrychenko.model.domain.Toppings;
import com.kyrychenko.model.pizzas.Ingredient;

public class IngredientsFactory {
    private static final double ONIONS_PRICE = 3;
    private static final double CHEESE_PRICE = 4;
    private static final double PEPPERS_PRICE = 4;
    private static final double SALAMI_PRICE = 5;
    private static final double MUSHROOMS_PRICE = 5;
    private static final double PEPPERONI_PRICE = 6;
    private static final double SAUSAGE_PRICE = 6;
    private static final double BACON_PRICE = 7;
    private static final double PINEAPPLE_PRICE = 10;

    public static Ingredient createCheeseIngredient() {
        return new Ingredient(Toppings.CHEESE, CHEESE_PRICE);
    }

    public static Ingredient createSalamiIngredient() {
        return new Ingredient(Toppings.SALAMI, SALAMI_PRICE);
    }

    public static Ingredient createBaconIngredient() {
        return new Ingredient(Toppings.BACON, BACON_PRICE);
    }

    public static Ingredient createMushroomsIngredient() {
        return new Ingredient(Toppings.MUSHROOMS, MUSHROOMS_PRICE);
    }

    public static Ingredient createOnionsIngredient() {
        return new Ingredient(Toppings.ONIONS, ONIONS_PRICE);
    }

    public static Ingredient createPepperoniIngredient() {
        return new Ingredient(Toppings.PEPPERONI, PEPPERONI_PRICE);
    }

    public static Ingredient createPeppersIngredient() {
        return new Ingredient(Toppings.PEPPERS, PEPPERS_PRICE);
    }

    public static Ingredient createPineappleIngredient() {
        return new Ingredient(Toppings.PINEAPPLE, PINEAPPLE_PRICE);
    }

    public static Ingredient createSausageIngredient() {
        return new Ingredient(Toppings.SAUSAGE, SAUSAGE_PRICE);
    }
}

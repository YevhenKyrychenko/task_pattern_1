package com.kyrychenko.model.factories;

import com.kyrychenko.model.pizzas.Crust;
import com.kyrychenko.model.pizzas.Ingredient;
import com.kyrychenko.model.pizzas.Pizza;
import com.kyrychenko.model.pizzas.Sauce;

import java.util.List;

public interface PizzaFactory {
    Pizza bakePepperoni();
    Pizza bakeCustomPizza(List<Ingredient> ingredients, Sauce sauce, Crust dough);
}

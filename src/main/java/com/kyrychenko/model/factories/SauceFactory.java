package com.kyrychenko.model.factories;

import com.kyrychenko.model.domain.SauceType;
import com.kyrychenko.model.pizzas.Sauce;

public class SauceFactory {
    private static final double MARINARA_PRICE = 2;
    private static final double PESTO_PRICE = 3;
    private static final double PLUM_TOMATO_PRICE = 1;

    public static Sauce createMarinaraSauce() {
        return new Sauce(SauceType.MARINARA, MARINARA_PRICE);
    }

    public static Sauce createPestoSauce() {
        return new Sauce(SauceType.PESTO, PESTO_PRICE);
    }

    public static Sauce createPlumTomatoSauce() {
        return new Sauce(SauceType.PLUM_TOMATO, PLUM_TOMATO_PRICE);
    }
}

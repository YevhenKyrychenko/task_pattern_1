package com.kyrychenko.model.factories;

import com.kyrychenko.model.domain.CrustType;
import com.kyrychenko.model.pizzas.Crust;

public class CrustFactory {
    private static double THIN_PRICE = 3;
    private static double THICK_PRICE = 4;

    public static Crust createThinCrust() {
        return new Crust(THIN_PRICE, CrustType.THIN);
    }

    public static Crust createThickCrust() {
        return new Crust(THICK_PRICE, CrustType.THICK);
    }
}

package com.kyrychenko.model;

import com.kyrychenko.model.Order;
import com.kyrychenko.model.pizzas.Pizza;

import java.util.List;

public interface PizzaOrderModelInterface {
    List<Pizza> preparePizzas(Order order);
}

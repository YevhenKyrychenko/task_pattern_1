package com.kyrychenko.model.orders;

import com.kyrychenko.model.domain.PizzaType;
import com.kyrychenko.model.pizzas.Crust;
import com.kyrychenko.model.pizzas.Ingredient;
import com.kyrychenko.model.pizzas.Sauce;

import java.util.List;

public class OrderItem {
    private PizzaType type;
    private int count;
    private List<Ingredient> ingredients;
    private Sauce sauce;
    private Crust crust;

    public OrderItem(PizzaType type, int count) {
        this.type = type;
        this.count = count;
    }

    public OrderItem(PizzaType type, int count, List<Ingredient> ingredients, Sauce sauce, Crust crust) {
        this.type = type;
        this.count = count;
        this.ingredients = ingredients;
        this.sauce = sauce;
        this.crust = crust;
    }

    public PizzaType getType() {
        return type;
    }

    public int getCount() {
        return count;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public Crust getCrust() {
        return crust;
    }
}
